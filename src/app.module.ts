import { Module } from '@nestjs/common';
import { KrakenApiModule } from './kraken-api/kraken-api.module';
import { DatabaseModule } from './database/database.module';

@Module({
    imports: [KrakenApiModule, DatabaseModule],
})
export class AppModule {}
