export * from './dataset.schema';
export * from './label.schema';

export enum DB {
    datasets = 'Datasets',
    label = 'Label',
}
