import { Schema } from 'mongoose';

export const DatasetSchema = new Schema({
    label: {
        type: String,
        unique: true,
        required: true,
    },
    data: [Number],
});
