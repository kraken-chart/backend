import { Document } from 'mongoose';

export interface Label extends Document {
    times: string[];
}
