export const Config = {
    kraken: {
        apiKey: process.env.KRAKEN_API_KEY,
        secret: process.env.KRAKEN_API_SECRET_KEY,
        endpoint: 'https://api.kraken.com',
        version: 0,
    },
    mongo: {
        host: process.env.KRAKEN_MONGODB_HOST,
    },
    currencies: {
        excluded: ['XETH', 'XXBT'],
    },
};
