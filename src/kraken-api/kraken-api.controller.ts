import { Controller, Get, Post } from '@nestjs/common';
import { KrakenApiService } from './kraken-api.service';
import { Dataset } from 'src/database/interfaces';

type ChartData = { labels: string[]; datasets: Dataset[] };

@Controller('kraken')
export class KrakenApiController {
    constructor(private readonly kraken: KrakenApiService) {}

    @Get()
    async getData(): Promise<ChartData> {
        return {
            labels: await this.kraken.labels(),
            datasets: await this.kraken.datasets(),
        };
    }

    @Post('update')
    async forceUpdate() {
        await this.kraken.updateChart();
    }

    @Post('total')
    async createTotals() {
        await this.kraken.updateTotals();
    }

    @Post('start')
    startCron() {
        this.kraken.startCron();
    }

    @Post('stop')
    stopCron() {
        this.kraken.stopCron();
    }
}
