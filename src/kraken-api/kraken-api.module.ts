import { Module, HttpModule } from '@nestjs/common';
import { KrakenApiService } from './kraken-api.service';
import { KrakenApiController } from './kraken-api.controller';
import { DatabaseModule } from 'src/database/database.module';
import { MongooseModule } from '@nestjs/mongoose';
import { DB, LabelSchema, DatasetSchema } from 'src/database/schemas';

@Module({
    imports: [
        HttpModule,
        DatabaseModule,
        MongooseModule.forFeature([
            {
                name: DB.label,
                schema: LabelSchema,
            },
            {
                name: DB.datasets,
                schema: DatasetSchema,
            },
        ]),
    ],
    providers: [KrakenApiService],
    exports: [KrakenApiService],
    controllers: [KrakenApiController],
})
export class KrakenApiModule {}
